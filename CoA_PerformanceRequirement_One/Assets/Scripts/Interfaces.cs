﻿namespace PROne
{
    public interface IAttack
    {
        void AttackEnemy(Enemy attackedEnemy, int attackDamage);
    }
    
    public interface IDamagable
    {
        void Damage(int damageAmount);
    }
    
    public interface ILog
    {
        void Log(object message);
    }
}