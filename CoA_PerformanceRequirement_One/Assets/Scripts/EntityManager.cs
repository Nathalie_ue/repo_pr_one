﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {
        public void Log(object message)
        {
            Debug.Log(message);
        }

        public Entity[] entities;

        private void Start()
        {
            Log("The names are being registered.");

            for (int i = 0; i < entities.Length; i++)
            {
                Log("Current counting: " + i + " " + entities[i].EntityName);
            }
        }
    }
}
