﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable, ILog
    {
        [SerializeField]
        private int currentHealth;
        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        [SerializeField]
        private int attackAmount;
        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }

        [SerializeField]
        private bool isAlive;
        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }


        public void Damage(int damageAmount)
        {
            if (currentHealth > 0)
            {
                currentHealth -= damageAmount;

                if (currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Log("The health of the " + EntityName + " is currently: " + currentHealth);
                    return;
                }
            }
            else
            {
                currentHealth = 0;
                isAlive = false;
                Log("The health of the " + EntityName + " is currently: " + currentHealth);
                return;
            }

            Log("The enemy " + EntityName + " has been damages, his health now: " + currentHealth);
        }
    }
}
